from ctypes.wintypes import RGB
import PySimpleGUI as sg
import time
import threading
import datetime
import os
from mutagen import File
import subprocess


class log:
    def error(ErrorDescription):
        print(f"エラー：{ErrorDescription}")


class FileAction:
    import sys
    import os

    def ConvertFileLink(CurrentFilePath):
        data = FileAction.os
        base = data.path.dirname(FileAction.sys.argv[0])
        name = data.path.normpath(data.path.join(base, CurrentFilePath))
        return name

    def Read(CurrentFilePath):
        path = FileAction.ConvertFileLink(CurrentFilePath)
        Files = open(path, "r", encoding="UTF-8")
        result = Files.read()
        Files.close()
        return result

    def Write(CurrentFilePath, WriteString, FileReadMode):
        path = FileAction.ConvertFileLink(CurrentFilePath)
        Files = open(path, FileReadMode, encoding="UTF-8")
        Files.write(WriteString)
        Files.close()

    def WriteAdd(CurrentFilePath, WriteString):
        FileAction.Write(CurrentFilePath, WriteString, "a")

    def WriteNew(CurrentFilePath, WriteString):
        FileAction.Write(CurrentFilePath, WriteString, "w")

    def Clear(CurrentFilePath):
        FileAction.WriteNew(CurrentFilePath, "")


class NowTime(threading.Thread):
    # 現在時刻出力ON/OFF
    power = False

    # 現在時刻(ミリ秒込み)
    NT = ""
    # 現在時刻(ミリ秒無し)
    NTS = ""

    # GUIの各種情報を格納するための変数
    # values = object

    def run(self):
        NowTime.power = True
        while NowTime.power == True:
            # 現在時刻を取得する
            L_NT = str(datetime.datetime.now()).replace('-', '/')[0:-4]
            L_NTS = L_NT[0:-3]
            DisplayOutput = L_NT
            FileOutput = L_NTS

            # チェックボックスの値を取得
            try:
                # ミリ秒の表示設定
                if values["IfMilliSecond"] == False:
                    DisplayOutput = L_NT[0:-3]

                # ファイル出力時の改行設定
                if values["IfLine"] == True:
                    FileOutput = FileOutput.replace(" ", "\n")
            except:
                break

            # 画面情報更新
            try:
                window["Times"].update(DisplayOutput)
            except:
                break

            # 秒が変わった時
            if NowTime.NTS != L_NTS:
                try:
                    FileAction.WriteNew("output\\NowTimes.txt", FileOutput)
                except:
                    os.mkdir(FileAction.ConvertFileLink("output"))
                    log.error("フォルダが存在しません。フォルダを作成します。")

            NowTime.NT = L_NT
            NowTime.NTS = L_NTS

            time.sleep(0.005)

        # ループを抜けた後の画面表示
        try:
            window["Times"].update("")
        except:
            return


class Button:
    def StartNowTime():
        if NowTime.power == False:
            # 現在時刻表示機能ON
            TimeUpdate = NowTime()
            TimeUpdate.start()

    def StopNowTime():
        # 現在時刻表示機能OFF
        NowTime.power = False

    def OLSMS():
        # ライブ配信管理画面を開く
        cmd("start https://studio.youtube.com/channel/UC/livestreaming")

    def OpenOBS():
        cmd('cd "C:\Program Files (x86)\Steam\steamapps\common\OBS Studio\bin\64bit" & start "" obs64.exe')


def cmd(Order):
    subprocess.call(Order, shell=True)


def cmd_ex(Order):
    try:
        subprocess.check_call(Order, shell=True)
        return True
    except:
        return False


# 画面構成
layout = [
    [sg.Text("各種起動ボタン")],
    [sg.HorizontalSeparator()],
    [sg.Checkbox("配信管理画面", size=(20, 1)),
     sg.Checkbox("OBS", size=(20, 1)),
     sg.Checkbox("現在時刻出力", size=(20, 1))],
    [sg.Button("ライブ配信管理画面を開く", key="OLSMS", button_color=(
        "black", "#64778D"), border_width=0, font=("", 10, "bold"))],
    [sg.Text("")],
    [sg.Text("現在時刻出力")],
    [sg.HorizontalSeparator()],
    [sg.Text("現在時刻\t："),
        sg.Text("", key="Times", size=(50, 1)),
        sg.Button("出力開始", key="StartNowTime", size=(15, 1)),
        sg.Button("出力停止", key="StopNowTime", size=(15, 1))],
    [sg.Text("出力設定\t："),
     sg.Checkbox("画面表示にミリ秒を含める", key="IfMilliSecond",
                 enable_events=True, default=True)],
    [sg.Text("\t\t　"),
     sg.Checkbox("ファイル出力時に改行を含める", key="IfLine",
                 enable_events=True, default=True)],
    [sg.Text("")],
    [sg.Text("YouTubeコメ欄リンクコンバーター", size=(30, 1))],
    [sg.HorizontalSeparator()],
    [sg.Text("URL\t\t："), sg.Input(size=(57, 1)),
     sg.Button("変換＆コメ欄URLコピー", size=(32, 1), key="InputURL")],
    [sg.Text("ﾀﾞｯｼｭﾎﾞｰﾄﾞURL\t："), sg.Input(size=(57, 1), disabled=True),
     sg.Button("コピー", size=(32, 1), key="DashboardURL")],
    [sg.Text("動画URL\t\t："), sg.Input(size=(57, 1), disabled=True),
     sg.Button("コピー", size=(32, 1), key="VideoURL")],
    [sg.Text("コメ欄URL\t："), sg.Input(size=(57, 1), disabled=True),
     sg.Button("コピー", size=(32, 1), key="CommentURL")],
    [sg.Button("a")]
]

# ウィンドウの生成
window = sg.Window("配信補助ツール", layout)

# イベントループ
while True:
    event, values = window.read()

    NowTime.values = values

    if event == sg.WIN_CLOSED:
        # ウィンドウが閉じられた時に処理を抜ける
        break
    elif event == "StartNowTime":
        Button.StartNowTime()
    elif event == "StopNowTime":
        Button.StopNowTime()
    elif event == "OLSMS":
        Button.OLSMS()
    elif event == "OpenOBS":
        Button.OpenOBS()
    elif event == "a":
        cmd_ex('cd "C:\Program Files\obs-studio\bin\64bit" & start "" obs64.exe')

window.close()
